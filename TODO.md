# Homenet TODO

## Kubernetes

https://kubernetes.io/blog/2023/08/31/legacy-package-repository-deprecation/
https://kubernetes.io/blog/2023/08/15/pkgs-k8s-io-introduction/

How to migrate to the Kubernetes community-owned repositories?
Debian, Ubuntu, and operating systems using apt/apt-get

Replace the apt repository definition so that apt points to the new repository instead of the Google-hosted repository. Make sure to replace the Kubernetes minor version in the command below with the minor version that you're currently using:

    echo "deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /" | sudo tee /etc/apt/sources.list.d/kubernetes.list

Download the public signing key for the Kubernetes package repositories. The same signing key is used for all repositories, so you can disregard the version in the URL:

    curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg

Update: In releases older than Debian 12 and Ubuntu 22.04, the folder /etc/apt/keyrings does not exist by default, and it should be created before the curl command.

Update the apt package index:

    sudo apt-get update



# gron
https://github.com/tomnomnom/gron

# podman
install newer version (debian)

# hledger

version is too old on Debian

# matplotlib
not idempotent, no check if already installed

# chrome
not idempotent, no check if already installed

# Jetbrains toolbox
not idempotent, no check if already installed

# nodejs

ansible_distribution does not work for debian testing
(workaround currently applied)

# Install recent version of httpie

```shell
# Install httpie

curl -SsL https://packages.httpie.io/deb/KEY.gpg | sudo gpg --dearmor -o /usr/share/keyrings/httpie.gpg

sudo echo "deb [arch=amd64 signed-by=/usr/share/keyrings/httpie.gpg] https://packages.httpie.io/deb ./" > /etc/apt/sources.list.d/httpie.list

sudo apt update

sudo apt install httpie
```

or use the snap


# Fonts

* Jetbrains Mono
* Apple San Francisco Mono
* Inconsolata



# Integrate AppImage launcher

sudo apt install software-properties-common
sudo add-apt-repository ppa:appimagelauncher-team/stable
sudo apt-get update
sudo apt-get install appimagelauncher


# Check tools

* DebOps - https://github.com/debops/debops


# fzf

apt package of fzf is kind of imcomplete (as of 2021-08), see 
    https://bugs.launchpad.net/ubuntu/+source/fzf/+bug/1928670

for now temporary fixed in .dotfiles

Also broken (for now): fzf vim




# robo3t is now robomongo

* see https://github.com/Studio3T/robomongo
* adjust role
* version upgrade


# mongosh

mongosh is the replacement for the now discontinued mongodb-client

See https://docs.mongodb.com/mongodb-shell/install/#std-label-mdb-shell-install

As of now, there are now packages in the ppa for Ubuntu 21.4



# Some Gnome tools

* install dconf-tools



# edit /etc/hosts

add entries for homenet machines


# check renameutils imv

# Kadeck

# Stoplight Studio?

# nomad

# kops

<https://github.com/kubernetes/kops>

# move GOROOT out of root directory

# conda module

check <https://github.com/UDST/ansible-conda>

# fix hcloud setup

download from <https://github.com/hetznercloud/cli/releases>


# cfssl

# aws

# .editorconfig in home dir might not be a good idea

# ensure that \`essential software\` has no personal dependencies

# BUG sdkman installation changes file in dotfiles (modified: shells/.zshrc)

# remove unrequired meta roles

-   developer-software
-   andy-home

# documentation: handling of PGP keys

# maybe "accept~hostkey~" could be used instead of adding host keys manually

# Misc


## enable unattended security updates

see https://serversforhackers.com/c/automatic-security-updates





