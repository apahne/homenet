# Homenet

# Hosts

## Bare Metal

| Hostname | IP             | Remark                 |
|----------|----------------|------------------------|
| snape    | 192.168.178.40 | Linux (main)           |
| sirius   | 192.168.178.47 | Macbook M1             |
| hagrid   | 192.168.178.29 | Linux (Dell XPS-15)    |
| tin      |                | Linux (small Thinkpad) |


## Virtual

| Hostname | IP              | Remark                |
|----------|-----------------|-----------------------|
| alastor  | 192.168.122.180 | Fedora virtual @snape |



# Required packages

ansible-galaxy collection install community.general