# Homenet DEVELOPER GUIDE

# Regular tasks

* check docker apt distribution release - {{ docker_apt_distribution_release }}
* check hashicorp apt distribution release - {{ hashicorp_apt_distribution_release }}
* update nodejs major version - {{ nodejs_version }}
* update Jetbrains toolbox version - {{ jetbrains_toolbox_version }}
* update helm - {{ helm_version }}
* update kafka cli version - {{ kafka_version }}
* update terragrunt - {{ terragrunt_version }}
* update sdkman java version - {{ java_version }}