# Homenet USER-GUIDE



## New Host Setup

Make sure that ssh is available on the new machine.

```
sudo apt install openssh-server
```

Also enable `multiverse`, if not yet done.

```
sudo apt-add-repository multiverse
```


You may want to disable automatic updates while the playbooks are running

```
sudo systemctl stop packagekit
```



## The Short Way - All In One

Everything in one sweep.

```
ansible-playbook \
    --extra-vars="target_host=hagrid" \
    --ask-vault-pass \
    --ask-become-pass \
    playbooks/site.yml
```


## The Short Way - All In Two

Everything in two sweep. First the regular, then the heavyweights.

```
ansible-playbook \
    --extra-vars="target_host=titan" \
    --ask-vault-pass \
    --ask-become-pass \
    --skip-tags="heavy" \
    playbooks/site.yml

ansible-playbook \
    --extra-vars="target_host=titan" \
    --ask-become-pass \
    --tags="heavy" \
    playbooks/site.yml
```

## Updates

```
ansible-playbook \
    --extra-vars="target_host=snape" \
    --ask-become-pass \
    --tags="helm,java,kafka-cli,nodejs,packer,terraform,terragrunt,vagrant" \
    playbooks/site.yml
```


## The Long Way - Individual Steps


Setup crypto identity

```
ansible-playbook \
    --extra-vars="target_host=hagrid" \
    --ask-vault-pass \
    --ask-become-pass \
    playbooks/andy-crypto-identity.yml
```


Setup user home - that requires the previous `crypto identity`

```
ansible-playbook \
    --extra-vars="target_host=titan" \
    --ask-become-pass \
    playbooks/andy-home.yml
```

Setup essential software (skipping latex, because that takes forever)

```
ansible-playbook \
    --extra-vars="target_host=titan" \
    --ask-become-pass \
    --skip-tags="heavy" \
    playbooks/essential-software.yml
```


And the development tools

```
ansible-playbook \
    --extra-vars="target_host=titan" \
    --ask-become-pass \
    playbooks/developer-software.yml
```

Now latex and other heavyweights - get a coffe

```
ansible-playbook \
    --extra-vars="target_host=titan" \
    --ask-become-pass \
    --tags="heavy" \
    playbooks/essential-software.yml
```



# Other examples

ansible-playbook \
    --extra-vars="target_host=hagrid" \
    --ask-vault-pass \
    --ask-become-pass \
    --tags=""   \
    playbooks/site.yml
