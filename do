#!/bin/bash
set -eu

HOMENET_SECRET_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#require_ansible_playbook {
#  type git >/dev/null 2>&1 || { echo >&2 "git not installed.  Aborting."; exit 1; }
#}

cfg_diff() {
  diff ansible.cfg <(curl https://raw.githubusercontent.com/ansible/ansible/devel/examples/ansible.cfg) || exit 0
}


all() {
    TARGET="${1}"
    echo Running complete playbook
    echo target = $TARGET
    ansible-playbook \
        --extra-vars="target_host=$TARGET" \
        --ask-become-pass \
        playbooks/site.yml
}

tags() {
    TARGET="${1}"
    TAGS="${2}"
    echo Running complete playbook
    echo target = $TARGET
    ansible-playbook \
        --extra-vars="target_host=$TARGET" \
        --ask-become-pass \
        --tags=$TAGS  \
        playbooks/site.yml
}


print_usage() {
    echo "\
Usage:
  ${0} [command]

Available Commands:

  all           perform complete playbook
                usage:
                      ${0} all target

  tags          perform complete playbook, apply tags
                usage:
                      ${0} tags target tags

  diff          show changes to upstream ansible.cfg template

"
}


cmd="${1:-}"
shift || true
case "$cmd" in
  all) all "${1:-}" ;;
  tags) tags "${1:-}" "${2:-}" ;;
  diff) cfg_diff ;;
  *) print_usage ;;
esac
